package com.cjgmj.testSpringAOP.service;

import com.cjgmj.testSpringAOP.entity.Empleado;

public interface EmpleadoService {

	public Long getNumEmpleado(Empleado emp);

	public Long getNumEmpleadoSinParametros();

	public Long getNumEmpNull(Long numEmp);

}
