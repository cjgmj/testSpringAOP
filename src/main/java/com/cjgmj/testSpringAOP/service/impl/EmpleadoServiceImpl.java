package com.cjgmj.testSpringAOP.service.impl;

import org.springframework.stereotype.Service;

import com.cjgmj.testSpringAOP.entity.Empleado;
import com.cjgmj.testSpringAOP.service.EmpleadoService;

@Service
public class EmpleadoServiceImpl implements EmpleadoService {

	private Long numEmp;

	@Override
	public Long getNumEmpleado(Empleado emp) {
		return emp.getNumEmpleado();
	}

	@Override
	public Long getNumEmpleadoSinParametros() {
		return this.getNumEmp();
	}

	@Override
	public Long getNumEmpNull(Long numEmp) {
		return numEmp;
	}

	public Long getNumEmp() {
		return numEmp;
	}

	public void setNumEmp(Long numEmp) {
		this.numEmp = numEmp;
	}

}
