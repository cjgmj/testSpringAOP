package com.cjgmj.testSpringAOP.aspect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjgmj.testSpringAOP.entity.Empleado;

@Aspect
@Component
public class AOPHandler {

	private final static Logger LOG = LoggerFactory.getLogger(AOPHandler.class);

	@Autowired
	private HttpServletRequest request;

	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.PostMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.PutMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.DeleteMapping)"
			+ "||@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void methods() {
	}

	@Pointcut("within(com.cjgmj.testSpringAOP.service..*)")
	public void services() {
	}

	@Before("services() && args(emp,..)")
	private void cambiarNumEmp(Empleado emp) {
		LOG.info("Nombre del empleado: " + emp.getNombre());
		emp.setNumEmpleado(new Long((int) (Math.random() * 999)));
		LOG.info("Número del empleado: " + emp.getNumEmpleado());
	}

//	@Before("services()")
//	private void cambiarVariableLocal(JoinPoint joinPoint) {
//		if (joinPoint.getTarget() instanceof EmpleadoServiceImpl) {
//			((EmpleadoServiceImpl) joinPoint.getTarget()).setNumEmp(new Long((int) (Math.random() * 999)));
//		}
//	}

	@Before("services()")
	private void cambiarVariableGenerica(JoinPoint joinPoint) {
		try {
			Class<?> c = joinPoint.getTarget().getClass();
			Method m = c.getDeclaredMethod("setNumEmp", Long.class);
			m.invoke(joinPoint.getTarget(), new Long((int) (Math.random() * 999)));
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Around("services() && args(numEmp,..)")
	private Object cambiarNumEmpNull(ProceedingJoinPoint pjp, Long numEmp) throws Throwable {
		LOG.info("Inicia " + pjp.getSignature().getName() + " con parámetro numEmp = " + numEmp);
		numEmp = new Long((int) (Math.random() * 999));
		Object ret = pjp.proceed(new Object[] { numEmp });
		LOG.info("Termina " + pjp.getSignature().getName() + " con parámetro numEmp = " + numEmp);
		return ret;
	}

	@Before("methods()")
	private void beforeMethods(JoinPoint joinPoint) {
		LOG.info("Valor de data: " + request.getHeader("data"));
	}

	@AfterReturning(value = "methods()", returning = "ret")
	private void afterReturningMethods(JoinPoint joinPoint, Object ret) {
		LOG.info("Valor devuelto: " + ret);
	}

	@AfterThrowing(value = "methods()", throwing = "e")
	private void afterThrowingMethods(JoinPoint joinPoint, Exception e) {
		LOG.info("El error ha sido: " + e.getMessage());
	}

}
