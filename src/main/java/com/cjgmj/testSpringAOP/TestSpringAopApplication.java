package com.cjgmj.testSpringAOP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpringAopApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSpringAopApplication.class, args);
	}

}
