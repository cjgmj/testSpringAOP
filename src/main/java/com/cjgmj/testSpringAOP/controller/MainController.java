package com.cjgmj.testSpringAOP.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cjgmj.testSpringAOP.entity.Empleado;
import com.cjgmj.testSpringAOP.service.EmpleadoService;

@RestController
@RequestMapping("")
public class MainController {

	@Autowired
	private EmpleadoService empleadoService;

	@GetMapping("/test")
	public String test(HttpServletRequest request) throws Exception {
		String data = request.getHeader("data");

		if (data.length() < 5) {
			throw new Exception("El tamaño de data es menor a 5");
		}

		return data;
	}

	@PostMapping("/numEmpleado")
	public Long getNumEmpleado(@RequestBody Empleado emp) {
		return empleadoService.getNumEmpleado(emp);
	}

	@GetMapping("/getNumEmp")
	public Long getNumEmp() {
		return empleadoService.getNumEmpleadoSinParametros();
	}

	@GetMapping("/getNumEmpNull")
	public Long getNumEmpNull() {
		return empleadoService.getNumEmpNull(null);
	}

}
