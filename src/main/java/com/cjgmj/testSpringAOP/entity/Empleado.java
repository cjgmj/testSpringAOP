package com.cjgmj.testSpringAOP.entity;

public class Empleado {

	private String nombre;
	private Long numEmpleado;

	public Empleado() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getNumEmpleado() {
		return numEmpleado;
	}

	public void setNumEmpleado(Long numEmpleado) {
		this.numEmpleado = numEmpleado;
	}

}
